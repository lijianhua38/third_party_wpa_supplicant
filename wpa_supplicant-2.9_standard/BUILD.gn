#Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")

WPA_ROOT_DIR = "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard"

declare_args() {
  wpa_supplicant_driver_nl80211_hisi = false
  wpa_supplicant_driver_nl80211 = false
  wpa_supplicant_ohos_certmgr = true
}

if (wpa_supplicant_driver_nl80211_hisi) {
  CONFIG_HISI = "true"
} else {
  CONFIG_HISI = "false"
}
if (wpa_supplicant_driver_nl80211) {
  CONFIG_DRIVER = "nl80211"
} else {
  CONFIG_DRIVER = "hdf"
}

CONFIG_OS = "unix"
CONFIG_ELOOP = "eloop"
if ("${CONFIG_DRIVER}" == "nl80211") {
  CONFIG_L2_PACKET = "linux"
} else {
  CONFIG_L2_PACKET = "rtos"
}
CONFIG_CTRL_IFACE = "udp"
CONFIG_MAIN = "main"

config("wpa_warnings") {
  cflags = [ "-Wno-error=sign-compare" ]
  ldflags = [
    "-flto",
    "-fsanitize=cfi",
    "-Wl,-plugin-opt,O1",
  ]
}

ohos_shared_library("wpa_client") {
  sanitize = {
    cfi = true
    debug = false
    blocklist = "./wpa_blocklist.txt"
  }

  output_name = "wpa_client"
  sources = [
    "$WPA_ROOT_DIR/src/common/wpa_ctrl.c",
    "$WPA_ROOT_DIR/src/utils/common.c",
    "$WPA_ROOT_DIR/src/utils/os_${CONFIG_OS}.c",
    "$WPA_ROOT_DIR/src/utils/wpa_debug.c",
  ]

  include_dirs = [
    "$WPA_ROOT_DIR/src",
    "$WPA_ROOT_DIR/src/utils",
    "$WPA_ROOT_DIR/src/drivers",
    "$WPA_ROOT_DIR/wpa_supplicant",
    "$WPA_ROOT_DIR/build/include",
    "//drivers/peripheral/wlan/client/include",
    "//third_party/bounds_checking_function/include",
    "//base/hiviewdfxhilog/interfaces/native/innerkits/include",
    "//base/startup/init/interfaces/innerkits/include/syspara",
  ]

  cflags = [
    "-UANDROID",
    "-fsigned-char",
    "-DCONFIG_IEEE80211W",
    "-DCONFIG_NO_VLAN",
    "-DCONFIG_NO_RADIUS",
    "-DCONFIG_NO_RANDOM_POOL",
    "-DCONFIG_SHA256",
    "-DCONFIG_CRYPTO_INTERNAL",
    "-DCONFIG_INTERNAL_LIBTOMMATH",
    "-DCONFIG_INTERNAL_SHA384",
    "-DCONFIG_INTERNAL_SHA512",
    "-DCONFIG_CTRL_IFACE",
    "-DCONFIG_CTRL_IFACE_UDP",
    "-DCONFIG_IBSS_RSN",
    "-DIEEE8021X_EAPOL",
    "-DCONFIG_BACEND_FILE",
    "-DCONFIG_NO_CONFIG_BLOBS",
    "-DUSERSPACE_CLIENT_SUPPORT",
    "-DCONFIG_DEBUG_FILE",
    "-DCONFIG_OPEN_HARMONY_PATCH",
  ]

  deps = [
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//base/startup/init/interfaces/innerkits:libbegetutil",
  ]

  if ("${CONFIG_DRIVER}" == "nl80211") {
    include_dirs += [ "$WPA_ROOT_DIR/libnl/include/libnl3" ]
    cflags += [
      "-DCONFIG_DRIVER_NL80211",
      "-DCONFIG_LIBNL32",
    ]
  } else {
    cflags += [
      "-DCONFIG_DRIVER_HDF",
      "-DCONFIG_OHOS_P2P",
    ]
  }
  configs = [ ":wpa_warnings" ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
  install_images = [
    "system",
    "updater",
  ]
}

wpa_base_sources = [
  # wpa & hostapd
  "$WPA_ROOT_DIR/hostapd/ap_config_file.c",
  "$WPA_ROOT_DIR/hostapd/ap_ctrl_iface.c",
  "$WPA_ROOT_DIR/hostapd/ap_eap_register.c",

  # hostapd
  "$WPA_ROOT_DIR/hostapd/ap_main.c",
  "$WPA_ROOT_DIR/src/ap/ap_config.c",
  "$WPA_ROOT_DIR/src/ap/ap_drv_ops.c",
  "$WPA_ROOT_DIR/src/ap/ap_list.c",
  "$WPA_ROOT_DIR/src/ap/ap_mlme.c",
  "$WPA_ROOT_DIR/src/ap/ap_rrm.c",
  "$WPA_ROOT_DIR/src/ap/authsrv.c",
  "$WPA_ROOT_DIR/src/ap/beacon.c",
  "$WPA_ROOT_DIR/src/ap/bss_load.c",
  "$WPA_ROOT_DIR/src/ap/ctrl_iface_ap.c",
  "$WPA_ROOT_DIR/src/ap/dfs.c",
  "$WPA_ROOT_DIR/src/ap/drv_callbacks.c",
  "$WPA_ROOT_DIR/src/ap/eap_user_db.c",
  "$WPA_ROOT_DIR/src/ap/hostapd.c",
  "$WPA_ROOT_DIR/src/ap/hw_features.c",
  "$WPA_ROOT_DIR/src/ap/ieee802_11.c",
  "$WPA_ROOT_DIR/src/ap/ieee802_11_auth.c",
  "$WPA_ROOT_DIR/src/ap/ieee802_11_ht.c",
  "$WPA_ROOT_DIR/src/ap/ieee802_11_shared.c",
  "$WPA_ROOT_DIR/src/ap/ieee802_1x.c",
  "$WPA_ROOT_DIR/src/ap/neighbor_db.c",
  "$WPA_ROOT_DIR/src/ap/pmksa_cache_auth.c",
  "$WPA_ROOT_DIR/src/ap/preauth_auth.c",
  "$WPA_ROOT_DIR/src/ap/sta_info.c",
  "$WPA_ROOT_DIR/src/ap/tkip_countermeasures.c",
  "$WPA_ROOT_DIR/src/ap/utils.c",
  "$WPA_ROOT_DIR/src/ap/wmm.c",
  "$WPA_ROOT_DIR/src/ap/wpa_auth.c",
  "$WPA_ROOT_DIR/src/ap/wpa_auth_glue.c",
  "$WPA_ROOT_DIR/src/ap/wpa_auth_ie.c",
  "$WPA_ROOT_DIR/src/common/ctrl_iface_common.c",
  "$WPA_ROOT_DIR/src/common/hw_features_common.c",
  "$WPA_ROOT_DIR/src/common/ieee802_11_common.c",
  "$WPA_ROOT_DIR/src/common/wpa_common.c",
  "$WPA_ROOT_DIR/src/crypto/aes-internal.c",
  "$WPA_ROOT_DIR/src/crypto/aes-omac1.c",
  "$WPA_ROOT_DIR/src/crypto/sha1-prf.c",
  "$WPA_ROOT_DIR/src/crypto/sha256-prf.c",
  "$WPA_ROOT_DIR/src/drivers/driver_common.c",
  "$WPA_ROOT_DIR/src/drivers/drivers.c",
  "$WPA_ROOT_DIR/src/eap_common/eap_common.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_methods.c",
  "$WPA_ROOT_DIR/src/eap_server/eap_server.c",
  "$WPA_ROOT_DIR/src/eap_server/eap_server_identity.c",
  "$WPA_ROOT_DIR/src/eap_server/eap_server_methods.c",
  "$WPA_ROOT_DIR/src/eapol_auth/eapol_auth_sm.c",
  "$WPA_ROOT_DIR/src/eapol_supp/eapol_supp_sm.c",
  "$WPA_ROOT_DIR/src/l2_packet/l2_packet_${CONFIG_L2_PACKET}.c",
  "$WPA_ROOT_DIR/src/rsn_supp/pmksa_cache.c",
  "$WPA_ROOT_DIR/src/rsn_supp/preauth.c",
  "$WPA_ROOT_DIR/src/rsn_supp/wpa.c",
  "$WPA_ROOT_DIR/src/rsn_supp/wpa_i.h",
  "$WPA_ROOT_DIR/src/rsn_supp/wpa_ie.c",
  "$WPA_ROOT_DIR/src/utils/${CONFIG_ELOOP}.c",
  "$WPA_ROOT_DIR/src/utils/bitfield.c",
  "$WPA_ROOT_DIR/src/utils/common.c",
  "$WPA_ROOT_DIR/src/utils/ip_addr.c",
  "$WPA_ROOT_DIR/src/utils/os_${CONFIG_OS}.c",
  "$WPA_ROOT_DIR/src/utils/radiotap.c",
  "$WPA_ROOT_DIR/src/utils/wpa_debug.c",
  "$WPA_ROOT_DIR/src/utils/wpabuf.c",
  "$WPA_ROOT_DIR/wpa_supplicant/${CONFIG_MAIN}.c",
  "$WPA_ROOT_DIR/wpa_supplicant/bss.c",
  "$WPA_ROOT_DIR/wpa_supplicant/bssid_ignore.c",

  # wpa_supplicant
  "$WPA_ROOT_DIR/wpa_supplicant/config.c",
  "$WPA_ROOT_DIR/wpa_supplicant/config_file.c",
  "$WPA_ROOT_DIR/wpa_supplicant/ctrl_iface.c",
  "$WPA_ROOT_DIR/wpa_supplicant/ctrl_iface_${CONFIG_CTRL_IFACE}.c",
  "$WPA_ROOT_DIR/wpa_supplicant/eap_register.c",
  "$WPA_ROOT_DIR/wpa_supplicant/events.c",
  "$WPA_ROOT_DIR/wpa_supplicant/ibss_rsn.c",
  "$WPA_ROOT_DIR/wpa_supplicant/notify.c",
  "$WPA_ROOT_DIR/wpa_supplicant/op_classes.c",
  "$WPA_ROOT_DIR/wpa_supplicant/robust_av.c",
  "$WPA_ROOT_DIR/wpa_supplicant/rrm.c",
  "$WPA_ROOT_DIR/wpa_supplicant/scan.c",
  "$WPA_ROOT_DIR/wpa_supplicant/wmm_ac.c",
  "$WPA_ROOT_DIR/wpa_supplicant/wpa_supplicant.c",
  "$WPA_ROOT_DIR/wpa_supplicant/wpas_glue.c",

  #sae
  "$WPA_ROOT_DIR/src/common/sae.c",
  "$WPA_ROOT_DIR/wpa_supplicant/sme.c",

  #p2p
  "$WPA_ROOT_DIR/src/ap/p2p_hostapd.c",
  "$WPA_ROOT_DIR/src/ap/wps_hostapd.c",
  "$WPA_ROOT_DIR/src/common/dragonfly.c",
  "$WPA_ROOT_DIR/src/common/gas.c",
  "$WPA_ROOT_DIR/src/crypto/crypto_openssl.c",
  "$WPA_ROOT_DIR/src/crypto/dh_groups.c",
  "$WPA_ROOT_DIR/src/crypto/fips_prf_openssl.c",
  "$WPA_ROOT_DIR/src/crypto/ms_funcs.c",
  "$WPA_ROOT_DIR/src/crypto/sha1-tlsprf.c",
  "$WPA_ROOT_DIR/src/crypto/sha256-kdf.c",
  "$WPA_ROOT_DIR/src/crypto/tls_openssl.c",
  "$WPA_ROOT_DIR/src/crypto/tls_openssl_ocsp.c",
  "$WPA_ROOT_DIR/src/eap_common/chap.c",
  "$WPA_ROOT_DIR/src/eap_common/eap_peap_common.c",
  "$WPA_ROOT_DIR/src/eap_common/eap_pwd_common.c",
  "$WPA_ROOT_DIR/src/eap_common/eap_sim_common.c",
  "$WPA_ROOT_DIR/src/eap_common/eap_wsc_common.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_aka.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_mschapv2.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_peap.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_pwd.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_sim.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_tls.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_tls_common.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_ttls.c",
  "$WPA_ROOT_DIR/src/eap_peer/eap_wsc.c",
  "$WPA_ROOT_DIR/src/eap_peer/mschapv2.c",
  "$WPA_ROOT_DIR/src/eap_server/eap_server_wsc.c",
  "$WPA_ROOT_DIR/src/p2p/p2p.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_build.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_dev_disc.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_go_neg.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_group.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_invitation.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_parse.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_pd.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_sd.c",
  "$WPA_ROOT_DIR/src/p2p/p2p_utils.c",
  "$WPA_ROOT_DIR/src/tls/bignum.c",
  "$WPA_ROOT_DIR/src/utils/base64.c",
  "$WPA_ROOT_DIR/src/utils/config.c",
  "$WPA_ROOT_DIR/src/utils/crc32.c",
  "$WPA_ROOT_DIR/src/utils/uuid.c",
  "$WPA_ROOT_DIR/src/wps/wps.c",
  "$WPA_ROOT_DIR/src/wps/wps_attr_build.c",
  "$WPA_ROOT_DIR/src/wps/wps_attr_parse.c",
  "$WPA_ROOT_DIR/src/wps/wps_attr_process.c",
  "$WPA_ROOT_DIR/src/wps/wps_common.c",
  "$WPA_ROOT_DIR/src/wps/wps_dev_attr.c",
  "$WPA_ROOT_DIR/src/wps/wps_enrollee.c",
  "$WPA_ROOT_DIR/src/wps/wps_registrar.c",
  "$WPA_ROOT_DIR/wpa_supplicant/ap.c",
  "$WPA_ROOT_DIR/wpa_supplicant/gas_query.c",
  "$WPA_ROOT_DIR/wpa_supplicant/offchannel.c",
  "$WPA_ROOT_DIR/wpa_supplicant/p2p_supplicant.c",
  "$WPA_ROOT_DIR/wpa_supplicant/p2p_supplicant_sd.c",
  "$WPA_ROOT_DIR/wpa_supplicant/wps_supplicant.c",
  "$WPA_ROOT_DIR/wpa_supplicant_lib/wpa_magiclink.c",
]

wpa_base_include_dirs = [
  "$WPA_ROOT_DIR/src",
  "$WPA_ROOT_DIR/src/utils",
  "$WPA_ROOT_DIR/src/drivers",
  "$WPA_ROOT_DIR/wpa_supplicant",
  "$WPA_ROOT_DIR/build/include",
  "$WPA_ROOT_DIR/wpa_supplicant_lib",
  "//third_party/libnl/include",
  "//drivers/peripheral/wlan/client/include",
  "//third_party/bounds_checking_function/include",
  "//third_party/openssl/include",
  "$WPA_ROOT_DIR/src/crypto",
  "$WPA_ROOT_DIR/src/eap_common",
  "//base/hiviewdfxhilog/interfaces/native/innerkits/include",
]

ohos_shared_library("wpa") {
  sanitize = {
    cfi = true
    debug = false
    blocklist = "./wpa_blocklist.txt"
  }
  output_name = "wpa"
  sources = wpa_base_sources

  include_dirs = wpa_base_include_dirs

  cflags = [
    "-UANDROID",
    "-fsigned-char",
    "-DCONFIG_IEEE80211W",
    "-DCONFIG_NO_VLAN",
    "-DCONFIG_NO_RADIUS",
    "-DCONFIG_NO_RANDOM_POOL",
    "-DCONFIG_SHA256",
    "-DCONFIG_CRYPTO_INTERNAL",
    "-DCONFIG_INTERNAL_LIBTOMMATH",
    "-DCONFIG_INTERNAL_SHA384",
    "-DCONFIG_INTERNAL_SHA512",
    "-DCONFIG_CTRL_IFACE",
    "-DCONFIG_CTRL_IFACE_UDP",
    "-DCONFIG_IBSS_RSN",
    "-DIEEE8021X_EAPOL",
    "-DUSERSPACE_CLIENT_SUPPORT",
    "-DCONFIG_BACKEND_FILE",
    "-DCONFIG_NO_CONFIG_BLOBS",
    "-DCONFIG_NO_ACCOUNTING",
    "-DEAP_SERVER_IDENTITY",
    "-DCONFIG_IEEE80211N",
    "-DHOSTAPD",
    "-DNEED_AP_MLME",
    "-DCONFIG_WPS",
    "-DCONFIG_AP",
    "-DCONFIG_P2P",
    "-DEAP_WSC",
    "-DEAP_SERVER_WSC",
    "-DEAP_SERVER",
    "-DCONFIG_GAS",
    "-DCONFIG_OFFCHANNEL",
    "-DCONFIG_MAGICLINK",
    "-DCONFIG_OPEN_HARMONY_PATCH",
    "-DEAP_MSCHAPV2",
    "-DEAP_TLS",
    "-DEAP_PEAP",
    "-DEAP_TTLS",
    "-DEAP_SIM",
    "-DEAP_PWD",
    "-DEAP_MSCHAPv2",
    "-DCONFIG_SHA256",
    "-DCONFIG_ECC",
    "-DEAP_AKA",
    "-DEAP_AKA_PRIME",
    "-DEAP_TLS_OPENSSL",
    "-DCONFIG_SAE",
    "-DCONFIG_SME",
    "-DCONFIG_WEP",
  ]

  defines = [
    "TLS_DEFAULT_CIPHERS = \"DEFAULT:!EXP:!LOW\"",
    "OPENSSL_SUPPRESS_DEPRECATED",
  ]

  deps = [
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//drivers/peripheral/wlan/client:wifi_driver_client",
    "//third_party/bounds_checking_function:libsec_static",
    "//third_party/openssl:libcrypto_shared",
    "//third_party/openssl:libssl_shared",
  ]
  external_deps = [ "init:libbegetutil" ]

  if ("${CONFIG_DRIVER}" == "nl80211") {
    sources += [
      "$WPA_ROOT_DIR/src/ap/ieee802_11_he.c",
      "$WPA_ROOT_DIR/src/ap/ieee802_11_vht.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_capa.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_event.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_monitor.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_scan.c",
      "$WPA_ROOT_DIR/src/drivers/linux_ioctl.c",
      "$WPA_ROOT_DIR/src/drivers/netlink.c",
      "$WPA_ROOT_DIR/src/drivers/rfkill.c",
    ]
    include_dirs += [
      "$WPA_ROOT_DIR/libnl/include/libnl3",
      "$WPA_ROOT_DIR/wpa_supplicant_lib",
    ]

    cflags += [
      "-DCONFIG_DRIVER_NL80211",
      "-DCONFIG_LIBNL32",
      "-DCONFIG_LIBNL20",
      "-DCONFIG_DEBUG_FILE",
      "-DCONFIG_IEEE80211AC",
      "-DCONFIG_IEEE80211AX",
    ]
    deps += [ "//third_party/libnl:libnl_share" ]
  } else {
    sources += [
      "$WPA_ROOT_DIR/src/drivers/wpa_hal.c",
      "$WPA_ROOT_DIR/src/drivers/wpa_hal_event.c",
    ]
    cflags += [
      "-DCONFIG_DRIVER_HDF",
      "-DCONFIG_OHOS_P2P",
    ]
  }
  if ("${CONFIG_HISI}" == "true") {
    sources += [ "$WPA_ROOT_DIR/wpa_supplicant_lib/driver_nl80211_hisi.c" ]
    cflags += [ "-DCONFIG_DRIVER_NL80211_HISI" ]
    if (use_musl) {
      cflags += [ "-DCONFIG_DRIVER_NL80211_HISI_TRUNK" ]
    }
  }

  if (wpa_supplicant_ohos_certmgr) {
    sources += [ "$WPA_ROOT_DIR/wpa_supplicant_lib/wpa_evp_key.c" ]
    deps += [ "//base/security/certificate_manager/interfaces/innerkits/cert_manager_standard/main:cert_manager_sdk" ]
    include_dirs += [
      "//base/security/certificate_manager/frameworks/cert_manager_standard/main/common/include",
      "//base/security/certificate_manager/interfaces/innerkits/cert_manager_standard/main/include",
      "//third_party/openssl",
    ]
    defines += [ "CONFIG_OHOS_CERTMGR" ]
  }

  configs = [ ":wpa_warnings" ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
}

ohos_shared_library("wpa_updater") {
  sanitize = {
    cfi = true
    debug = false
    blocklist = "./wpa_blocklist.txt"
  }
  output_name = "wpa_updater"
  sources = wpa_base_sources

  include_dirs = wpa_base_include_dirs

  cflags = [
    "-UANDROID",
    "-fsigned-char",
    "-DCONFIG_IEEE80211W",
    "-DCONFIG_NO_VLAN",
    "-DCONFIG_NO_RADIUS",
    "-DCONFIG_NO_RANDOM_POOL",
    "-DCONFIG_SHA256",
    "-DCONFIG_CRYPTO_INTERNAL",
    "-DCONFIG_INTERNAL_LIBTOMMATH",
    "-DCONFIG_INTERNAL_SHA384",
    "-DCONFIG_INTERNAL_SHA512",
    "-DCONFIG_CTRL_IFACE",
    "-DCONFIG_CTRL_IFACE_UDP",
    "-DCONFIG_IBSS_RSN",
    "-DIEEE8021X_EAPOL",
    "-DUSERSPACE_CLIENT_SUPPORT",
    "-DCONFIG_BACKEND_FILE",
    "-DCONFIG_NO_CONFIG_BLOBS",
    "-DCONFIG_NO_ACCOUNTING",
    "-DEAP_SERVER_IDENTITY",
    "-DCONFIG_IEEE80211N",
    "-DHOSTAPD",
    "-DNEED_AP_MLME",
    "-DCONFIG_WPS",
    "-DCONFIG_AP",
    "-DCONFIG_P2P",
    "-DEAP_WSC",
    "-DEAP_SERVER_WSC",
    "-DEAP_SERVER",
    "-DCONFIG_GAS",
    "-DCONFIG_OFFCHANNEL",
    "-DCONFIG_MAGICLINK",
    "-DCONFIG_OPEN_HARMONY_PATCH",
    "-DEAP_MSCHAPV2",
    "-DEAP_TLS",
    "-DEAP_PEAP",
    "-DEAP_TTLS",
    "-DEAP_SIM",
    "-DEAP_PWD",
    "-DEAP_MSCHAPv2",
    "-DCONFIG_SHA256",
    "-DCONFIG_ECC",
    "-DEAP_AKA",
    "-DEAP_AKA_PRIME",
    "-DEAP_TLS_OPENSSL",
    "-DCONFIG_SAE",
    "-DCONFIG_SME",
    "-DCONFIG_WEP",
  ]

  defines = [
    "TLS_DEFAULT_CIPHERS = \"DEFAULT:!EXP:!LOW\"",
    "OPENSSL_SUPPRESS_DEPRECATED",
  ]

  deps = [
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//drivers/peripheral/wlan/client:wifi_driver_client",
    "//third_party/bounds_checking_function:libsec_static",
    "//third_party/openssl:libcrypto_shared",
    "//third_party/openssl:libssl_shared",
  ]
  external_deps = [ "init:libbegetutil" ]

  if ("${CONFIG_DRIVER}" == "nl80211") {
    sources += [
      "$WPA_ROOT_DIR/src/ap/ieee802_11_he.c",
      "$WPA_ROOT_DIR/src/ap/ieee802_11_vht.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_capa.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_event.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_monitor.c",
      "$WPA_ROOT_DIR/src/drivers/driver_nl80211_scan.c",
      "$WPA_ROOT_DIR/src/drivers/linux_ioctl.c",
      "$WPA_ROOT_DIR/src/drivers/netlink.c",
      "$WPA_ROOT_DIR/src/drivers/rfkill.c",
    ]
    include_dirs += [
      "$WPA_ROOT_DIR/libnl/include/libnl3",
      "$WPA_ROOT_DIR/wpa_supplicant_lib",
    ]

    cflags += [
      "-DCONFIG_DRIVER_NL80211",
      "-DCONFIG_LIBNL32",
      "-DCONFIG_LIBNL20",
      "-DCONFIG_DEBUG_FILE",
      "-DCONFIG_IEEE80211AC",
      "-DCONFIG_IEEE80211AX",
    ]
    deps += [ "//third_party/libnl:libnl_share" ]
  } else {
    sources += [
      "$WPA_ROOT_DIR/src/drivers/wpa_hal.c",
      "$WPA_ROOT_DIR/src/drivers/wpa_hal_event.c",
    ]
    cflags += [
      "-DCONFIG_DRIVER_HDF",
      "-DCONFIG_OHOS_P2P",
    ]
  }
  if ("${CONFIG_HISI}" == "true") {
    sources += [ "$WPA_ROOT_DIR/wpa_supplicant_lib/driver_nl80211_hisi.c" ]
    cflags += [ "-DCONFIG_DRIVER_NL80211_HISI" ]
    if (use_musl) {
      cflags += [ "-DCONFIG_DRIVER_NL80211_HISI_TRUNK" ]
    }
  }
  symlink_target_name = [ "libwpa.z.so" ]
  configs = [ ":wpa_warnings" ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
  install_images = [ "updater" ]
}

ohos_executable("wpa_cli") {
  sources = [
    "$WPA_ROOT_DIR/src/common/cli.c",
    "$WPA_ROOT_DIR/src/common/wpa_ctrl.c",
    "$WPA_ROOT_DIR/src/utils/${CONFIG_ELOOP}.c",
    "$WPA_ROOT_DIR/src/utils/common.c",
    "$WPA_ROOT_DIR/src/utils/edit_simple.c",
    "$WPA_ROOT_DIR/src/utils/os_${CONFIG_OS}.c",
    "$WPA_ROOT_DIR/src/utils/wpa_debug.c",
    "$WPA_ROOT_DIR/wpa_supplicant/wpa_cli.c",
  ]

  include_dirs = [
    "$WPA_ROOT_DIR/src",
    "$WPA_ROOT_DIR/src/utils",
    "$WPA_ROOT_DIR/src/drivers",
    "$WPA_ROOT_DIR/wpa_supplicant",
    "$WPA_ROOT_DIR/build/include",
    "//base/hiviewdfxhilog/interfaces/native/innerkits/include",
    "//base/startup/init/interfaces/innerkits/include/syspara",
  ]

  cflags = [
    "-UANDROID",
    "-fsigned-char",
    "-DCONFIG_IEEE80211W",
    "-DCONFIG_NO_VLAN",
    "-DCONFIG_NO_RADIUS",
    "-DCONFIG_NO_RANDOM_POOL",
    "-DCONFIG_SHA256",
    "-DCONFIG_CRYPTO_INTERNAL",
    "-DCONFIG_INTERNAL_LIBTOMMATH",
    "-DCONFIG_INTERNAL_SHA384",
    "-DCONFIG_INTERNAL_SHA512",
    "-DCONFIG_CTRL_IFACE",
    "-DCONFIG_CTRL_IFACE_UDP",
    "-DCONFIG_IBSS_RSN",
    "-DIEEE8021X_EAPOL",
    "-DUSERSPACE_CLIENT_SUPPORT",
    "-DCONFIG_BACKEND_FILE",
    "-DCONFIG_NO_CONFIG_BLOBS",
    "-DCONFIG_WPS",
    "-DCONFIG_AP",
    "-DCONFIG_P2P",
    "-DEAP_WSC",
    "-DEAP_SERVER_WSC",
    "-DEAP_SERVER",
    "-DCONFIG_GAS",
    "-DCONFIG_OFFCHANNEL",
    "-DCONFIG_MAGICLINK",
    "-DCONFIG_DEBUG_FILE",
    "-DCONFIG_OPEN_HARMONY_PATCH",
  ]

  if ("${CONFIG_DRIVER}" == "nl80211") {
    include_dirs += [ "$WPA_ROOT_DIR/libnl/include/libnl3" ]
    cflags += [
      "-DCONFIG_DRIVER_NL80211",
      "-DCONFIG_LIBNL32",
    ]
  } else {
    cflags += [
      "-DCONFIG_DRIVER_HDF",
      "-DCONFIG_OHOS_P2P",
    ]
  }

  deps = [
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//base/startup/init/interfaces/innerkits:libbegetutil",
  ]

  configs = [ ":wpa_warnings" ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
}

ohos_executable("hostapd_cli") {
  sources = [
    "$WPA_ROOT_DIR/hostapd/hostapd_cli.c",
    "$WPA_ROOT_DIR/src/common/cli.c",
    "$WPA_ROOT_DIR/src/common/wpa_ctrl.c",
    "$WPA_ROOT_DIR/src/utils/${CONFIG_ELOOP}.c",
    "$WPA_ROOT_DIR/src/utils/common.c",
    "$WPA_ROOT_DIR/src/utils/edit_simple.c",
    "$WPA_ROOT_DIR/src/utils/os_${CONFIG_OS}.c",
    "$WPA_ROOT_DIR/src/utils/wpa_debug.c",
  ]

  include_dirs = [
    "$WPA_ROOT_DIR/src",
    "$WPA_ROOT_DIR/src/utils",
    "$WPA_ROOT_DIR/src/drivers",
    "$WPA_ROOT_DIR/wpa_supplicant",
    "$WPA_ROOT_DIR/build/include",
    "$WPA_ROOT_DIR/libnl/include/libnl3",
    "//base/hiviewdfxhilog/interfaces/native/innerkits/include",
    "//base/startup/init/interfaces/innerkits/include/syspara",
  ]

  cflags = [
    "-UANDROID",
    "-fsigned-char",
    "-DCONFIG_IEEE80211W",
    "-DCONFIG_NO_VLAN",
    "-DCONFIG_NO_RADIUS",
    "-DCONFIG_NO_RANDOM_POOL",
    "-DCONFIG_SHA256",
    "-DCONFIG_CRYPTO_INTERNAL",
    "-DCONFIG_INTERNAL_LIBTOMMATH",
    "-DCONFIG_INTERNAL_SHA384",
    "-DCONFIG_INTERNAL_SHA512",
    "-DCONFIG_CTRL_IFACE",
    "-DCONFIG_CTRL_IFACE_UDP",
    "-DCONFIG_IBSS_RSN",
    "-DIEEE8021X_EAPOL",
    "-DUSERSPACE_CLIENT_SUPPORT",
    "-DCONFIG_BACKEND_FILE",
    "-DCONFIG_NO_CONFIG_BLOBS",
    "-DCONFIG_NO_ACCOUNTING",
    "-DEAP_SERVER_IDENTITY",
    "-DCONFIG_IEEE80211N",
    "-DHOSTAPD",
    "-DNEED_AP_MLME",
    "-DCONFIG_WPS",
    "-DCONFIG_AP",
    "-DCONFIG_P2P",
    "-DEAP_WSC",
    "-DEAP_SERVER_WSC",
    "-DEAP_SERVER",
    "-DCONFIG_GAS",
    "-DCONFIG_OFFCHANNEL",
    "-DCONFIG_DEBUG_FILE",
    "-DCONFIG_OPEN_HARMONY_PATCH",
  ]

  deps = [
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//base/startup/init/interfaces/innerkits:libbegetutil",
  ]

  if ("${CONFIG_DRIVER}" == "nl80211") {
    cflags += [
      "-DCONFIG_DRIVER_NL80211",
      "-DCONFIG_LIBNL32",
    ]
  } else {
    cflags += [
      "-DCONFIG_DRIVER_HDF",
      "-DCONFIG_OHOS_P2P",
    ]
  }
  configs = [ ":wpa_warnings" ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
}

ohos_executable("wpa_supplicant") {
  sources = [ "$WPA_ROOT_DIR/wpa_test/wpa_sample.c" ]

  include_dirs = [ "$WPA_ROOT_DIR/build/include" ]

  deps = [ "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard:wpa" ]
  configs = [ ":wpa_warnings" ]
  cflags = [
    "-DCONFIG_DEBUG_FILE",
    "-DCONFIG_OPEN_HARMONY_PATCH",
  ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
}

ohos_executable("hostapd") {
  sources = [ "$WPA_ROOT_DIR/hostapd_test/hostapd_sample.c" ]

  include_dirs = [ "$WPA_ROOT_DIR/build/include" ]

  deps = [ "//third_party/wpa_supplicant/wpa_supplicant-2.9_standard:wpa" ]

  configs = [ ":wpa_warnings" ]
  cflags = [
    "-DCONFIG_DEBUG_FILE",
    "-DCONFIG_OPEN_HARMONY_PATCH",
  ]
  part_name = "wpa_supplicant-2.9"
  subsystem_name = "wpa_supplicant-2.9"
}
